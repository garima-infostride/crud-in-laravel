@extends('products.layout')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Add New Products</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{  route('products.index') }}">Back</a>
        </div>
    </div>
</div>

@if($errors->any())
<div class="alert alert-danger">
    <strong>Oopss!</strong> There were some problems with your input. <br><br>
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form action="{{  route('products.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="row align-center">
        <div class="col-xs-8 col-sm-8 col-md-8">
            <div class="form-group">
                <strong>Name: </strong>
                <input type="text" name="name" class="form-control" placeholder=" Enter product name">
            </div>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8">
            <div class="form-group">
                <strong>Detail: </strong>
                <textarea name="details" style="height:150px" class="form-control" placeholder="Detail...."></textarea>
            </div>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8">
            <div class="form-group">
                <strong>Image: </strong>
                <input type="file" name="image" class="form-control">
            </div>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 text-center">
            <div class="form-group">
                <button type="submit" class="btn btn-primary">Submit</button>    
            </div>
        </div>
    </div>
</form>

@endsection
@extends('products.layout')

@section('content')

<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h2>Edit Product</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('products.index') }}">Back</a>
        </div>
    </div>
</div>

@if($errors->any())
    <div class="alert alert-danger">
        <strong>Oops !</strong>There were some problem with your inputs. <br><br>
        <ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{ route('products.update', $product->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 ">
            <div class="form-group">
                <strong>Name :</strong>
                <input type="text" name="name" value="{{ $product->name }}" class="form-control" placeholder="Name" >
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 ">
            <div class="form-group">
                <strong>Detail :</strong>
                <textarea name="details" class="form-control" style="height: 150px" placeholder="Detail">{{ $product->details }}</textarea>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 ">
            <div class="form-group">
                <strong>Image :</strong>
                <input type="file" name="image" class="form-control">
                <img src="{{ asset('uploads/product/'.$product->image) }}"  width="80px" height="65px">
            </div> 
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">
            <button type="submit" class="btn btn-primary" >Submit</button>
        </div>
    </div>
</form>
@endsection
<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::latest()->paginate(3);

        return view('products.index', compact('products'))->with(request()->input('page'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate the inputs
        $request->validate([
            'name'=>'required',
            'details'=>'required',
            // 'image'=>'required',
        ]); 

        //Create a new product
        $product = new Product;
        $product->name = $request->input('name');
        $product->details = $request->input('details');
        if($request->hasfile('image'))
        {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/product/',$filename);
            $product->image = $filename;
        }
        $product->save();
        

        //redirect the user and send message
        return redirect()->route('products.index')->with('success','Product Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        // return view('products.show', compact('product'));
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //validate the inputs
        $request->validate([
            'name'=>'required',
            'details'=>'required',
        ]); 

        //update a product
        // $product->update($request->all());
        $product->name = $request->input('name');
        $product->details = $request->input('details');
        if($request->hasfile('image'))
        {
            $destination = 'uploads/product'.$product->image;
            if(File::exists($destination))
            {
                File::delete($destination);
            }
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time().'.'.$extension;
            $file->move('uploads/product/',$filename);
            $product->image = $filename;
        }
        $product->update();

        //redirect the user and send message
        return redirect()->route('products.index')->with('success','Product updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //detele the product
        $destination = 'uploads/product/'.$product->image;
        if(File::exists($destination))
        {
            File::delete($destination);
        }
        $product->delete();

        //redirect to the user and show success message 
        return redirect()->route('products.index')->with('success','Product deleted Successfully');
    }
}
